import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useState } from "react";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { FormControl, FormLabel } from "@mui/material";
import { Box } from "@mui/system";

const DEFAULT_FORM_VALUE = {
  title: "",
  start: new Date(),
  end: new Date(),
};

export default function FormDialog({ isOpen, handleClose, submit }) {
  const [formValue, setFormValue] = useState(DEFAULT_FORM_VALUE);

  const handleFormValue = (e) => {
    const {
      target: { name, value },
    } = e;
    const newFormValue = {
      ...formValue,
      [name]: value,
    };
    console.log(newFormValue);
    setFormValue(newFormValue);
  };

  const handleChangeDate = (name, value) => {
    setFormValue({
      ...formValue,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    submit(formValue);
  }

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Create new event</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here.
            We will send updates occasionally.
          </DialogContentText> */}
          <Box marginBottom={2}>
          <FormControl fullWidth>
            <TextField
              autoFocus
              margin="dense"
              name="title"
              label="Title"
              type="text"
              fullWidth
              variant="standard"
              onChange={handleFormValue}
            />
          </FormControl>
          </Box>
          <Box sx={{
            display: 'flex',
            gap: '20px'
          }}>
          <LocalizationProvider dateAdapter={AdapterMoment}>
            <FormControl>
              <DatePicker
                label="Start"
                value={formValue.start}
                onChange={(newValue) => {
                  handleChangeDate("start", newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </FormControl>

            <FormControl>

              <DatePicker
                label="End"
                value={formValue.end}
                onChange={(newValue) => {
                  handleChangeDate("end", newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </FormControl>

          </LocalizationProvider>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Subscribe</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
