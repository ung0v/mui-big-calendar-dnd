import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import listEvent from "../utils/events";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { Button } from "@mui/material";
import { Box } from "@mui/system";
import { useState } from "react";
import FormDialog from "../components/formDialog";
// Setup the localizer by providing the moment (or globalize, or Luxon) Object
// to the correct localizer.
const localizer = momentLocalizer(moment); // or globalizeLocalizer
export default function Home() {
  const [isOpen, setIsOpen] = useState(false);
  const [events, setEvents] = useState(listEvent);

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  const handleSubmit = (formValue) => {
    console.log(formValue);
    setEvents([...events, formValue]);
  }

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "20px",
      }}
    >
      <Button
        sx={{
          marginLeft: "auto",
        }}
        variant="contained"
        onClick={handleOpen}
      >
        Create event
      </Button>
      <Calendar
        localizer={localizer}
        events={events}
        defaultView="month"
        defaultDate={new Date(2015, 3, 1)}
        style={{ height: "100vh" }}
      />
      <FormDialog isOpen={isOpen} handleClose={handleClose} submit={handleSubmit}/>
    </Box>
  );
}
